package org.example;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        System.out.println("Hello World!");

        ArrayList<String> collection1 = new ArrayList<>();
        collection1.add("Anjing");
        collection1.add("Burung");
        collection1.add("Domba");
        collection1.add("Elang");

        ArrayList<String> collection2 = new ArrayList<>();
        collection2.add("Anjing");
        collection2.add("Cicak");

        ArrayList<String> collection3 = new ArrayList<>(collection1);
        ArrayList<String> collection4 = new ArrayList<>(collection2);

        collection3.retainAll(collection4);
        collection4.removeAll(collection3);
        collection1.addAll(collection4);
        collection1.removeAll(collection3);

        for (String hasil : collection1) {
            System.out.println(hasil);
        }
    }

}



