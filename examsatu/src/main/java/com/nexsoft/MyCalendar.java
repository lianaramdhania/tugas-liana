package com.nexsoft;

import java.util.Scanner;

public class MyCalendar {
    public void bismillah() {
//Create a new scanner
        Scanner input = new Scanner(System.in);

        // Meminta Pengguna Memasukkan Tahun
        System.out.print("Masukkan Tahun: ");
        int tahun = input.nextInt();

        // Meminta Pengguna Memasukkan Hari Awal
        System.out.print("Masukkan Hari Awal: ");
        int firstDayYear = input.nextInt();

        // A for loop that prints out each month
        for(int month = 1; month <= 12; month++)
        {
            // Set the value of the amount of days in a month
            int daysMonth = 0;

            // Set value of the month
            String monthDisplay = "";

            // Find name of each month and number of days
            switch(month)
            {
                case 1: monthDisplay = "January";
                    daysMonth = 31;
                    break;

                case 2:
                    monthDisplay = "February";
                    int tahunA = 0;
                    while (tahunA > -1)
                    {
                        // Count all years that are divisible by 4 to be a leap year.
                        tahunA += 4;

                        // If the year inputted is a leap year, the days of the month will be 29.
                        if (tahun == tahunA)
                        {
                            daysMonth = 29;
                            break;
                        }

                        else
                        {
                            daysMonth = 28;
                        }
                    }
                    break;

                case 3: monthDisplay = "Maret";
                    daysMonth = 31;
                    break;

                case 4: monthDisplay = "April";
                    daysMonth = 30;
                    break;

                case 5: monthDisplay = "Mei";
                    daysMonth = 31;
                    break;

                case 6: monthDisplay = "Juni";
                    daysMonth = 30;
                    break;

                case 7: monthDisplay = "Juli";
                    daysMonth = 31;
                    break;

                case 8: monthDisplay = "Agustus";
                    daysMonth = 31;
                    break;

                case 9: monthDisplay = "September";
                    daysMonth = 30;
                    break;

                case 10: monthDisplay = "Oktober";
                    daysMonth = 31;
                    break;

                case 11: monthDisplay = "November";
                    daysMonth = 30;
                    break;

                case 12: monthDisplay = "Desember";
                    daysMonth = 31;
                    break;

                // If the month is not recognized, dialog box will be displayed, and then exits program.
                default : System.out.print("Invalid: Your month is not recognized. ");
                    System.exit(0);

            }
            // Display the month and year
            System.out.println("                      "+ monthDisplay + " " + tahun);

            // Display the lines
            System.out.println("_____________________________________");

            // Display the days of the week
            System.out.println("M     S     S     R     K     J     S");

            // Print spaces depending on the day the month starts.
            int firstDayEachMonth = (daysMonth + firstDayYear)%7;
            for (int space = 1; space <= firstDayEachMonth; space++)
                System.out.print("   ");

            // Print the days
            for (int daysDisplay = 1; daysDisplay <= daysMonth; daysDisplay++)
            {
                if (firstDayYear%7 == 0)
                    System.out.println();

                System.out.printf("%3d      ", daysDisplay);
                firstDayYear += 1;
            }
            System.out.println();
        }

    }
}
